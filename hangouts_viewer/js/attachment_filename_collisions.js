"use strict";

const collisionFilenames = new Map(
    /*{{placeholder: collision map}}*/
);

const filenameOf = s => s.split('/').at(-1);

const showCollisionAlternatives = () => {
    $(".link-to-local-image")
        .filter((i, $e) => collisionFilenames.has(filenameOf($e.href)))
        .replaceWith(function() {
            const origFilename = filenameOf(this.href);
            return "<ul>".concat(
                [origFilename, ...collisionFilenames.get(origFilename)]
                    .map(fname => `<li><a href="attachments/Photos/${
                                      encodeURIComponent(fname)
                                      }">${escapeHTML(fname)}</a></li>`)
                    .join(''),
                "</ul>");
        });
};

const htmlSpecialChars = /[&<>"']/g;

const htmlEscapeCodes = new Map([
    ["&", "&amp;"],
    ["<", "&lt;"],
    [">", "&gt;"],
    ['"', "&quot;"],
    ["'", "&#39;"],
]);

const escapeHTML = s => s.replaceAll(htmlSpecialChars,
                                     m => htmlEscapeCodes.get(m));

$(document).ready(showCollisionAlternatives);
