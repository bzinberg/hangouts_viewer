import jinja2
import json
import pathlib
import pkgutil
import re
from .parse import *


_jinja2_env = jinja2.Environment(
    loader=jinja2.PackageLoader("hangouts_viewer"),
    autoescape=jinja2.select_autoescape())

index_template = _jinja2_env.get_template("index.html")
conversation_heading_template = _jinja2_env.get_template(
                                    "conversation_heading.html")
conversation_template = _jinja2_env.get_template("conversation.html")


def build_static_site(conversations, output_dir : pathlib.Path):
    """Creates a static site to display the supplied conversations.  After
    calling this function, copy over the attachments as described below.

    The directory structure of the site is:

        <output_dir>/
            index.html
            css/
                <stylesheets>
            js/
                <scripts>
            conversations/
                <conversation_id>.html
                attachments/

    Here `conversations` is a list of "conversation" objects, in the format of
    `/conversations` of the Google Hangouts export.

    After calling this function, you will probably want to copy the contents of
    the `Hangouts` directory of the Google Takeout dump (the directory that
    contains `Hangouts.json`, and also contains message attachments) to

        <output_dir>/conversations/attachments/

    The pages in the generated site assume that any locally-stored message
    attachments are in that directory.
    """
    for dirname in ("css", "js"):
        (output_dir / dirname).mkdir(parents=False, exist_ok=True)
    for relpath in ("css/index.css", "css/conversation.css",
                    "js/jquery-3.6.1.slim.min.js"):
        content = pkgutil.get_data("hangouts_viewer", relpath)
        logging.info(f"Writing static file {relpath}")
        with open(output_dir / relpath, "wb") as f:
            f.write(content)

    convs_dir = output_dir / "conversations"
    convs_dir.mkdir(parents=False, exist_ok=True)
    entries_for_index = []
    for _conv in conversations:
        conv_id = _conv["conversation"]["conversation"]["id"]["id"]
        logger.info(f"Reading conversation {conv_id}")

        (name_map, self_uid, msgs) = conversation_from_json(_conv)
        entries_for_index.append((conv_id,
                                  [name for (uid, name) in name_map.items()
                                   if uid != self_uid]))
        output = conversation_template.render(
                name_map=name_map,
                msgs=msgs,
                conversation_heading=conversation_heading_template)
        with open(convs_dir / f"{conv_id}.html", "w") as f:
            print(output, file=f, end="")
    logger.info("Building index")
    index_contents = index_template.render(entries=entries_for_index)
    with open(output_dir / "index.html", "w") as f:
        print(index_contents, file=f, end="")
    logger.info("Done.")


def gen_js_for_collision_alternatives(collision_map):
    """Returns the text of an ES6 script to enhance the rendered appearance of
    `PlusPhotoBlock`s by showing more guesses at which local file could
    correspond to the image attachment.

    The argument `collision_map` is as returned by `find_filename_collisions`:
    that is, a map whose keys are original filenames (e.g. `foo.png`) and whose
    value at such a key is a list of the filenames that look like de-collided
    forms of the key (e.g. `foo(1).png`).

    The returned script assumes jQuery is already loaded, and attaches a
    listener to perform the changes when the DOM tree is ready (jQuery `ready`
    event).

    The rendering improvement is as follows: if there are several local files
    that correspond to the attachment `foo.png`, then a message that has
    `foo.png` attached will link to not only the remote copy of the correct
    `foo.png` and the local copy of the maybe-correct file called `foo.png`,
    but also to the other local candidates such as `foo(1).png`.  This
    improvement is especially helpful in the case where a user has sent several
    image attachments in a row, each of which is named `<YYYY-MM-DD>.png`: this
    way, it does not look like several copies of the same image.

    Note that (as warned in the generated page) it is not guaranteed that the
    correct attachment will even be one of the local files.  The bundling and
    naming of message attachments into the Google Takeout dump is not well done
    and I am not aware of a systematic way to tell which dumped file (if any)
    an attachment described in `Hangouts.json` corresponds to without dialing
    into a remote server.  A future version of this library may provide a way
    to bulk-download attachments and name them systematically so that this
    explicitly ambiguous image rendering is not necessary.
    """
    js_template = pkgutil.get_data("hangouts_viewer",
                                   "js/attachment_filename_collisions.js"
                                   ).decode()
    placeholder_text = "/*{{placeholder: collision map}}*/"
    return js_template.replace(placeholder_text,
                               json.dumps(list(collision_map.items())))


# Regex for the end portion of a filename that matches the (not very
# sophisticated) Google Takeout convention for handling filename collisions.
# Examples of such names include `abcd(1)` and `abcd(1).png`.  The end portion
# of interest consists of a collision infix like `(1)` followed optionally by a
# file extension like `.png`.
COLLISION_PATTERN = re.compile(
        # Collision infix
        r"(?P<collision_infix>\([0-9]+\))"
        # Optional file extension
        r"(?P<extension>(\.[A-Za-z]*)?)"
        # Anchor to end of the string
        r"$")
def find_filename_collisions(filenames):
    """Returns a dict whose values are lists of elements of `filenames` that
    appear to be the Google Takeout dedup-ification of a common original
    filename; that original filename is the key.

    For example:

    >>> find_filename_collisions([
    ...     "abcd.png",
    ...     "efgh(1).png",
    ...     "efgh(2).png",
    ...     "ijkl(2)",
    ...     "ijkl",
    ... ])
    {'efgh.png': ['efgh(1).png', 'efgh(2).png'], 'ijkl': ['ijkl(2)']}
    """
    s = {}
    for fname in filenames:
        # `orig_fname` is what `fname` becomes when the collision infix is
        # removed
        (orig_fname, num_subs) = re.subn(COLLISION_PATTERN, "\g<extension>",
                                         fname)
        if num_subs > 0:
            if orig_fname not in s:
                s[orig_fname] = []
            s[orig_fname].append(fname)
    return s
