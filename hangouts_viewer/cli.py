"""Command-line interface for the `hangouts_viewer` package.

Example invocation:
    
    python3 -m hangouts_viewer build \
            --input_dir=/path/to/hangouts_export \
            --output_dir=/path/to/my_output_dir

Note that above, `/path/to/hangouts_export` is the directory that contains
`Hangouts.json`, which is a couple levels down from the root of the zip/tarball
that Google Takeout gives you.
"""

import argparse
import json
import logging
import os
import shutil
import pathlib
from .build_static_site import *
from .constants import *


def main():
    p = argparse.ArgumentParser(prog="hangouts_viewer",
                                description=("Create a static site to browse "
                                             "exported Google Hangouts "
                                             "history."),
                                allow_abbrev=False)
    p.add_argument("--verbose", "-v", action="store_true",
                   help="Enable verbose logging")
    p.add_argument("--very_verbose", "-V", action="store_true",
                   help="Enable very verbose logging")

    subparsers = p.add_subparsers(required=True, metavar="COMMAND",
                                  help=("For help on a specific subcommand, "
                                        f"run: {p.prog} <COMMAND> -h"))

    p_build = subparsers.add_parser("build", help="Build static site")
    p_build.set_defaults(func=run_build)
    p_build.add_argument("--input_dir", type=pathlib.Path, required=True,
                         help=("Directory within the exported data that "
                               "contains `Hangouts.json` and the message "
                               "attachments"))
    p_build.add_argument("--output_dir", type=pathlib.Path, required=True,
                         help=("Path to be the root of the built static site. "
                               "Will be created if doesn't exist, but at "
                               "least the parent directory must exist."))

    args = p.parse_args()

    logging.basicConfig(level=(logging.DEBUG if args.very_verbose
                               else logging.INFO if args.verbose
                               else logging.WARNING))

    args.func(args)


def run_build(args):
    args.input_dir = args.input_dir.expanduser()
    args.output_dir = args.output_dir.expanduser()

    assert args.input_dir.is_dir(), "input_dir must exist and be a directory"
    if not args.output_dir.exists():
        assert args.output_dir.parent.exists(), (
                "Parent directory of output_dir doesn't exist, please create "
                "it first")
        args.output_dir.mkdir()
    assert args.output_dir.is_dir(), "output_dir must be a directory"

    logging.info("Reading Hangouts.json")
    with open(args.input_dir / "Hangouts.json", "r") as f:
        _toplevel_obj = json.load(f)

    assert _toplevel_obj.keys() == {"conversations"}
    all_conversations = _toplevel_obj["conversations"]

    logging.info("Building static site")
    build_static_site(all_conversations, args.output_dir)

    logging.info("Copying message attachments to static site")
    attachments_dir = (args.output_dir / "conversations" / ATTACHMENTS_DIRNAME)
    shutil.copytree(args.input_dir,
                    attachments_dir,
                    dirs_exist_ok=True,
                    ignore=shutil.ignore_patterns("Hangouts.json"))

    logging.info("Creating attachment_filename_collisions.js")
    collisions = find_filename_collisions(
                     sorted(os.listdir(attachments_dir / "Photos")))
    script = gen_js_for_collision_alternatives(collisions)
    with open(args.output_dir / "js" / "attachment_filename_collisions.js",
              "w") as f:
        print(script, file=f, end="")

    print(f"Static site created at {args.output_dir}")
    logging.info("Done.")
