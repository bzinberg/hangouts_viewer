from collections import namedtuple
from datetime import datetime
import urllib.parse
from .logging import *
from .block import *


class UserID(namedtuple("_UserID", ["type", "content"])):
    """Hashable ID for a user, constructed from the info available in
    `/conversation/participant_data/<int>/id` of the Google Takeout dump.
    """
    def __str__(self):
        """Returns a slightly more human-readable form of a user ID, without
        worrying about stuff like escaping parentheses or preventing
        collisions.
        """
        return f"{self.type}({self.content})"

    @staticmethod
    def from_pdid(pdid):
        """Returns a hashable ID for a user, constructed from the info
        available in `/conversation/participant_data/<N>/id`.

        Will not have collisions unless the JSON data has collisions."""
        type_preference_order = ("gaia_id", "chat_id")
        for t in type_preference_order:
            if t in pdid:
                return UserID(type=t, content=pdid[t])
        raise Exception("No known ID types appeared for this user", pdid)


def name_map_from_pd(pd):
    """Returns a mapping of `{UserID: name}` for the participants in a
    conversation, constructed from `/conversation/participant_data`.
    """
    return {UserID.from_pdid(user["id"]):
            user.get("fallback_name",
                     f"Unnamed user {UserID.from_pdid(user['id'])}")
            for user in pd}


def segment_to_block(seg):
    """Converts a Hangouts message "segment" in the JSON format of Google
    Takeout to a simpler format (see `Block`) that this library can understand
    and render.
    """
    if seg["type"] == "TEXT":
        return TextBlock(text=seg["text"],
                         formatting=Formatting(**seg.get("formatting", {})))
    elif seg["type"] == "LINE_BREAK":
        return LineBreakBlock()
    elif seg["type"] == "LINK":
        return LinkBlock(target=seg["link_data"]["link_target"],
                         text=seg["text"],
                         formatting=Formatting(**seg.get("formatting", {})))
    else:
        raise Exception("Unrecognized message segment type", seg["type"])


def attachment_to_block(atch):
    """Converts a Hangouts message attachment in the JSON format of Google
    Takeout to a simpler format (see `Block`) that this library can understand
    and render.
    """
    if "embed_item" in atch:
        embed = atch["embed_item"]
        if embed["type"] == ["PLUS_PHOTO"]:
            url = embed["plus_photo"]["url"]
            fname = urllib.parse.urlparse(url).path.rsplit("/", maxsplit=1)[-1]
            # As far as I can see, the filename in the URL is *double*
            # URL-encoded.  For example, `=` becomes not `%3D` but `%253D`.  If
            # that's correct, then this is a bug in either the Takeout export
            # or the backend that names the remote files, depending on how you
            # look at it.
            #
            # Decoding once to work around the bug.
            fname = urllib.parse.unquote(fname)
            # Also, it appears that when the filename contains question marks,
            # the question marks are replaced with underscores in the Google
            # Takeout export.  Working around that:
            fname = fname.replace("%3F", "_")
            return PlusPhotoBlock(remote_url=url,
                                  filename=fname)
        elif "PLACE_V2" in embed["type"]:
            return PlaceBlock(name=embed["place_v2"].get("name", "<no name>"),
                              url=embed["place_v2"].get("url", "<missing url>"))
        elif "THING_V2" in embed["type"]:
            return ThingBlock(name=embed["thing_v2"].get("name", "<no name>"),
                              url=embed["thing_v2"].get("url", "<missing url>"))
        elif embed["type"] == ["DYNAMITE_MESSAGE_METADATA"]:
            return DynamiteMetadataBlock()
        else:
            raise Exception("Unrecognized embed type", embed["type"])
    else:
        raise Exception("I don't understand this attachment", atch)


class Message(namedtuple("_Message", ["timestamp", "sender_id", "blocks"])):
    """Simple representation of a chat message, including timestamp, sender,
    and contents (a list of `Block`s whose concatenation is the full contents).
    """
    pass


def conversation_from_json(_conv):
    """Converts conversation `_conv`, in the Google Takeout JSON format, into a
    simpler data structure that this library can understand and render.

    Returns:
        name_map: Mapping of `{UserID: name}` (see `name_map_from_pdid`)
        self_id: UserID of the user whose history dump this is (should be the
                 same for all conversations in `Hangouts.json`).
        msgs: List of `Message`s, one per message in the conversation.
    """
    metadata = _conv["conversation"]["conversation"]  # hindsight is 20/20
    events = _conv["events"]
    conv_id = metadata["id"]["id"]
    self_uid = UserID.from_pdid(metadata["self_conversation_state"][
                                    "self_read_state"][ "participant_id"])

    name_map = name_map_from_pd(metadata["participant_data"])
    logger.debug(f"Name map: {name_map}")
    def name_of(uid):
        return name_map.get(uid, f"Outside user {uid}")

    if metadata["type"] in ("STICKY_ONE_TO_ONE", "GROUP"):
        msgs = []
        for event in events:
            assert event["conversation_id"]["id"] == conv_id
            sender_id = UserID.from_pdid(event["sender_id"])
            sender_name = name_of(sender_id)
            ts = datetime.fromtimestamp(int(event["timestamp"][:-6]))
            if event["event_type"] == "REGULAR_CHAT_MESSAGE":
                _msg = event["chat_message"]["message_content"]
                blocks = []
                if "segment" in _msg:
                    blocks.extend([segment_to_block(seg)
                                   for seg in _msg["segment"]])
                if "attachment" in _msg:
                    # TODO run a check to see whether the file always exists
                    # when doing this construction from my data
                    blocks.extend([attachment_to_block(atch)
                                  for atch in _msg["attachment"]])
                if len(blocks) == 0:
                    raise Exception("What the heck is in this message?", _msg)
                msgs.append(Message(timestamp=ts, sender_id=sender_id,
                                    blocks=blocks))
            elif event["event_type"] == "HANGOUT_EVENT":
                subtype = HangoutEventType[
                              event["hangout_event"]["event_type"]]
                # `media_type` isn't always present
                # https://gitlab.com/bzinberg/hangouts_viewer/-/issues/1
                _media_type = event["hangout_event"].get("media_type",
                                                         "UNKNOWN")
                # Let's not crash if the `media_type` is present but
                # unrecognized; but let's at least log an error.
                if _media_type in HangoutMediaType.__members__:
                    media_type = HangoutMediaType[_media_type]
                else:
                    logger.error(f"Unrecognized media_type {_media_type}")
                    media_type = HangoutMediaType.UNKNOWN
                msgs.append(Message(timestamp=ts,
                                    sender_id=sender_id,
                                    blocks=[HangoutEventBlock(
                                                event_type=subtype,
                                                media_type=media_type)]))
            elif event["event_type"] in ("ADD_USER", "REMOVE_USER"):
                mchange = event["membership_change"]
                mchange_type = MembershipChangeType[mchange["type"]]
                affected_users = [UserID.from_pdid(pdid)
                                  for pdid in mchange["participant_id"]]
                blocks = [MembershipChangeBlock(
                              type=mchange_type,
                              uids_and_names=[(uid, name_of(uid))
                                              for uid in affected_users])]
                msgs.append(Message(timestamp=ts, sender_id=sender_id,
                                    blocks=blocks))
            elif event["event_type"] == "RENAME_CONVERSATION":
                r = event["conversation_rename"]
                old_name = r.get("old_name", "<missing name>")
                new_name = r.get("new_name", "<missing name>")
                msgs.append(Message(timestamp=ts, sender_id=sender_id,
                                    blocks=[RenameConversationBlock(
                                                old_name=old_name,
                                                new_name=new_name)]))
            elif event["event_type"] == "GROUP_LINK_SHARING_MODIFICATION":
                new_setting = event["group_link_sharing_modification"].get(
                        "new_status", "<missing group link sharing setting>")
                msgs.append(Message(timestamp=ts, sender_id=sender_id,
                                    blocks=[GroupLinkSharingChangeBlock(
                                                new_setting=new_setting)]))
            else:
                raise Exception("Unrecognized event type", event["event_type"])
        return (name_map, self_uid, sorted(msgs, key=lambda m: m.timestamp))
    else:
        raise Exception("Unrecognized conversation type", metadata["type"])
