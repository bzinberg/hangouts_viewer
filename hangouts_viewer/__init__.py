"""Tool for building a static site to view exported Google Hangouts history.

This tool was needed because the Hangouts history available from Google Takeout
is in a verbose JSON format that is not human-readable.  When run as a script
(`python3 -m`), the tool creates a static HTML site displaying the
conversations in human-readable form.
"""

from .block import *
from .build_static_site import *
from .constants import *
from .logging import *
from .parse import *
