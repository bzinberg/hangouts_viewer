from abc import ABC
from collections import namedtuple
from enum import Enum
from markupsafe import Markup
import pathlib
from .constants import *

class Formatting(namedtuple("_Formatting",
                            ["bold", "italics", "underline", "strikethrough"],
                            defaults=[False, False, False, False])):
    """Named tuple whose field names match those of the `"formatting"` objects
    in the Google Takeout JSON.
    """
    pass

_formatting_to_html = {"bold": "b",
                       "italics": "i",
                       "underline": "u",
                       "strikethrough": "strike"}
def apply_formatting(text, formatting):
    """Returns HTML markup in which the given formatting is applied to the
    given text.

    If `text` is an ordinary string (not a `markupsafe.Markup`), it is assumed
    to be HTML-unsafe, and will be escaped in the returned object.
    """
    fdict = formatting._asdict()
    tags = [html_tag for (prop, html_tag) in _formatting_to_html.items()
            if fdict[prop]]
    tags_open = Markup("").join(Markup(f"<{t}>") for t in tags)
    tags_close = Markup("").join(Markup(f"</{t}>") for t in tags[::-1])
    return tags_open + text + tags_close


def html_annotate_username(name, uid):
    """Returns HTML markup for a username with the user ID in the tooltip."""
    return (Markup("<abbr title=\"")
            + f"User ID: {uid}"
            + Markup("\">")
            + name
            + Markup("</abbr>"))


class Block(ABC):
    """Abstract base class for a "block" -- a renderable piece of a message.

    In this library, the contents of a message are represented as a sequence of
    `Block`s, which can be text, images, joining/leaving events, etc.  The
    `Block` classes themselves do not dictate how they should be rendered, and
    are not inherently tied to a specific templating engine, though elsewhere
    in the library we define mappings from `Block` types to Jinja2 template
    elements.
    """
    pass

class TextBlock(namedtuple("_TextBlock", ["text", "formatting"])):
    def __html__(self):
        return apply_formatting(self.text, self.formatting)

class LineBreakBlock(namedtuple("_LineBreakBlock", [])):
    def __html__(self):
        return Markup("<br />")

class LinkBlock(namedtuple("_LinkBlock", ["target", "text", "formatting"])):
    def __html__(self):
        return (Markup("<a target=\"_blank\" href=\"")
                + self.target
                + Markup("\">")
                + apply_formatting(self.text, self.formatting)
                + Markup("</a>"))

class HangoutEventType(Enum):
    START_HANGOUT = 1
    END_HANGOUT = 2

class HangoutMediaType(Enum):
    UNKNOWN = 0
    AUDIO_VIDEO = 1
    AUDIO_ONLY = 2

class HangoutEventBlock(namedtuple("_HangoutEventBlock",
                                   ["event_type", "media_type"])):
    def __html__(self):
        return (Markup("<div class=\"hangout-event\">")
                + { HangoutEventType.START_HANGOUT: "Started ",
                    HangoutEventType.END_HANGOUT: "Ended ",
                  }[self.event_type]
                + { HangoutMediaType.UNKNOWN: "a call",
                    HangoutMediaType.AUDIO_VIDEO: "a video call",
                    HangoutMediaType.AUDIO_ONLY: "an audio call",
                  }[self.media_type]
                + Markup("</div>"))

class MembershipChangeType(Enum):
    JOIN = 1
    LEAVE = 2

class MembershipChangeBlock(namedtuple("_MembershipChangeBlock",
                                       ["type", "uids_and_names"])):
    """Block indicating a change of membership in a group conversation (users
    joining or leaving).

    The field `uids_and_names` is a list of tuples of the form
    `(user_id, user_name)`, where `user_id` is in the format of `id_from_pdid`.
    """
    def __html__(self):
        return (Markup("<div class=\"membership-change\">")
                + { MembershipChangeType.JOIN: "User(s) joined: ",
                    MembershipChangeType.LEAVE: "User(s) left: ",
                  }[self.type]
                + Markup(", ").join(html_annotate_username(name, uid)
                                    for (uid, name) in self.uids_and_names)
                + Markup("</div>"))

class GroupLinkSharingChangeBlock(namedtuple("_GroupLinkSharingChangeBlock",
                                             ["new_setting"])):
    """Block indicating a change of group link sharing setting.

    I don't know what group link sharing is/was in Google Hangouts.  Oh well,
    unlikely to find out now that it's sunsetted.
    """
    def __html__(self):
        return (Markup("<div class=\"group-link-sharing-change\">"
                       "Changed group link sharing setting to: ")
                + {
                    "LINK_SHARING_OFF": "Off",
                    "LINK_SHARING_ON": "On",
                  }.get(self.new_setting,
                        # If there's no display name for the new setting, just
                        # show the codename -- it's probably human-readable
                        # upper snake case.
                        self.new_setting)
                + Markup("</div>"))

class RenameConversationBlock(namedtuple("_RenameConversationBlock",
                                         ["old_name", "new_name"])):
    """Block indicating a member has renamed the conversation.

    The fields `old_name` and `new_name` are either strings or `None`.
    """
    def __html__(self):
        return (Markup("<div class=\"rename-conversation\">")
                + f"Renamed the conversation from \"{self.old_name}\" "
                  f"to \"{self.new_name}\""
                + Markup("</div>"))

class PlusPhotoBlock(namedtuple("_PlusPhotoBlock",
                                ["remote_url", "filename"])):
    def __html__(self):
        img_path = str(pathlib.Path(ATTACHMENTS_DIRNAME) / "Photos" /
                       self.filename)
        return (Markup("<div class=\"preview-image-container\">"
                       "<p>Image, which may exist locally as one of:</p>"
                       "<a class=\"link-to-local-image\" target=\"_blank\" "
                       "href=\"")
                + img_path
                + Markup("\"><img class=\"preview-image\" src=\"")
                + img_path
                + Markup("\" /></a>"
                         "<p>The above might be the wrong file(s).  To know "
                         "for sure, see the <a target=\"_blank\" href=\"")
                + self.remote_url
                + Markup("\">remote copy</a> of the image (might not stay "
                         "up forever).</p>"
                         "</div>"))

class PlaceBlock(namedtuple("_PlaceBlock", ["name", "url"])):
    """Embedded "Place," or in Google Takeout parlance, `PLACE_V2`."""
    def __html__(self):
        link_text = (self.name if self.name != "" else self.url)
        return (Markup("<div class=\"embed-place\">"
                       "Place: <a target=\"_blank\" href=\"")
                + self.url
                + Markup("\">")
                + link_text
                + Markup("</a></div>"))

class ThingBlock(namedtuple("_ThingBlock", ["name", "url"])):
    """Embedded "Thing," or in Google Takeout parlance, `THING_V2`."""
    def __html__(self):
        link_text = (self.name if self.name != "" else self.url)
        return (Markup("<div class=\"embed-thing\">"
                       "<abbr=\"In Google parlance, THING_V2\">Thing</abbr>: "
                       "<a target=\"_blank\" href=\"")
                + self.url
                + Markup("\">")
                + link_text
                + Markup("</a></div>"))

class DynamiteMetadataBlock(namedtuple("_DynamiteMetadataBlock", [])):
    """Embedded Google Chat (old codename: Dynamite) metadata.

    As far as I have seen, to do anything useful with this metadata, you would
    need to make API calls into Google Chat, which is a completely different
    service from Google Hangouts.  However, messages that have Google Chat
    metadata attached are (as far as I have seen) always preceded by a Hangouts
    message that links to a webpage where the Google Chat content can be
    accessed, and Google Chat will continue to be available when Google
    Hangouts is turned down.
    """
    def __html__(self):
        return Markup("<div class=\"embed-dynamite-metadata\">"
                      "(Google Chat metadata attached)"
                      "</div>")

for cls in (TextBlock, LineBreakBlock, LinkBlock, HangoutEventBlock,
            MembershipChangeBlock, PlusPhotoBlock, PlaceBlock, ThingBlock,
            DynamiteMetadataBlock):
    Block.register(cls)
