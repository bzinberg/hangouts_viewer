# Hangouts Viewer

This repo contains a Python library and command-line tool to generate a static
site from the Hangouts dump from Google Takeout.

## Background

Google Hangouts will be turned down soon as it has been replaced by the new
Google Chat.  The current schedule is that Hangouts messages will stop being
searchable in Gmail on 2022 Nov 1, and will stop being downloadable via Google
Takeout on 2023 Jan 1.  Google has advised that all users who wish to save
their history download it from Google Takeout before then.

However, the dump available from Google Takeout is not human-readable.  It is a
large JSON file with lots of information that is extraneous for the purpose of
a user who wants to save and view their past messages.  (See below for an
example of how a 1-line message is represented as 42 lines of JSON.)

## What this tool does

This tool creates a static site with an index listing all conversations and a
page for each conversation.

| Index page | Conversation view |
| :--------: | :---------------: |
| ![Index page](docs/images/index_example.png) | ![Conversation view](docs/images/conversation_example.png) |

## How to use

1. First, download your Hangouts history from Google Takeout.  Extract the zip
   or tarball and find the subdirectory (a couple levels down) called
   `Hangouts`.  This directory should contain a JSON file called
   `Hangouts.json`, as well as many other files (copies of a subset of the
   attachments that appear in your Hangouts messages).

2. Now install the Python package in this repo.  You may want to create a fresh
   virtualenv first:
   ```shell
   python3 -m venv my_venv
   source my_venv/bin/activate
   ```
   Make sure you have relatively recent versions of `pip` and `setuptools`:
   ```shell
   python3 -m pip install --upgrade pip setuptools
   ```
   Then install the package: from the root directory of the repo, run
   ```shell
   python3 -m pip install .
   ```

3. Finally, run the command-line tool:
   ```shell
   python3 -m hangouts_viewer build \
       --input_dir=/path/to/Hangouts \
       --output_dir=./my_static_site
   ```
   The above invocation reads the Hangouts dump at `/path/to/Hangouts` and
   builds a static site rooted at `./my_static_site`.

### Notes on the static site

#### Time zones

I think the message timestamps are in the user's local time.  I don't know how
that choice is determined -- maybe a Google account setting, maybe a guess
based on the time zone the user's browser or chat client most frequently
reported while logged in, maybe the time zone of the user's browser when they
requested the Takeout dump.  The reason I think it's local time is that my data
has timestamps in Eastern time -- so it's not the case that all users get
timestamps in UTC (would have probably been the most reasonable choice) or
Mountain View time (a classic piece of Google tech debt).

## Known limitations

### Some users' names are missing

The static site builder uses the `fallback_name` field when it is available;
that's where the display names come from when they are present.  I don't know
what determines which users show up in the Takeout dump with a `fallback_name`
and which don't.  In my case, the vast majority of users did have
`fallback_name`s.

At the end of the day, users are "really" identified by their
[Gaia ID](https://stackoverflow.com/questions/27028322/how-to-look-up-user-information-from-google-gaia-id)s.
I think the only way to look up the non-"fallback" names is to send API
requests containing Gaia IDs to some other Google service.

### Some of the image files are wrong or missing

Yeah.  Well, sort of.  There is always a link to the remote copy of the image,
which is the right image but is hosted on some kind of Hangouts (or Google
Plus?) service that may be turned down soon.  However, not all images that
occur as attachments are included in the Takeout dump.  In my case, it appears
that images sent by somone else are not included.

In addition, the Takeout dump puts all the attachments into the same directory
(actually two directories, see "Why does the Takeout dump contain two copies of
every attachment" below), and as far as I can see, does not handle name
collisions in a reasonable way.  Well, it doesn't clobber all but one of the
files, but it renames them to things like `foo(1).png` in such a way that, as
far as I can tell, if a message has an attachment whose filename is `foo.png`,
there is no way to tell whether that corresponds to `foo.png`, `foo(1).png`,
`foo(2).png`, etc. in the Takeout dump.  It could also be none of those, e.g.
if both you and the other user sent images called `foo.png` and the other
user's attachments are not included in the dump.  This is called out in the
static site via blocks that look like this:

> Image, which may exist locally as one of:
>
> * today.png
> * today(1).png
>
> The above might be the wrong file(s). To know for sure, see the
> [remote copy](https://path.to/today.png) of the image (might not stay up
> forever).

Luckily, the links to remote copies of the attachments seem to all be correct.
A future version of this library may provide a way to bulk download attachments
from the remote server, but that would be a little complicated to do because
the client needs to be authenticated.  I'd probably implement it as printing
out a list of URLs for the user to then manually feed into a browser extension
like DownThemAll! for bulk download, then hopefully-automatically organizing
those files into a directory structure that disambiguates which remote URL a
given local file came from.

### I developed this using limited example data

So far, I have used only my own Hangouts history dump and the bug reports of
other users to develop and debug this package.  (Understandably, no one has
particularly wanted to share their full Hangouts history with me for debugging
purposes.)  I am not aware of there being any documentation or known-correct
example data to develop or test on.  It is likely that users of this library
will encounter crashes complaining of "I don't recognize this type of message,"
etc., when attempting to run it on their own data.  Please do file an issue if
you come across that.


## Future improvements

I'd like to implement the bulk downloading feature described above, so that the
correct versions of the attachments are available locally.  This would
presumably have to be done before the service is turned down, but who knows.
Also, I might not implement it.


## Other notes

### Why does the Takeout dump contain two copies of every attachment

I don't know.

In my case at least, `Hangouts/` and `Hangouts/Photos/` contain almost the
exact same non-exhaustive set of message attachments.  The differences are that
the files in `Hangouts/` always have file extensions (in fact, are always
`.png`, `.jpg` or `.gif`) whereas some files in `Hangouts/Photos/` (as well as
their remote counterparts) don't have extensions; and, in cases where two
attachment have both the same name _and_ the same contents, that file appears
twice in `Hangouts/Photos/` and only once in `Hangouts/`.

But basically, I don't know.

### What a single message looks like in JSON

The message is `hello, this is my message`.  Can you find it in the 42-line
JSON blob below?

```json
{
  "conversation_id": {
    "id": ...
  },
  "sender_id": {
    "gaia_id": ...,
    "chat_id": ...
  },
  "timestamp": ...,
  "self_event_state": {
    "user_id": {
      "gaia_id": ...,
      "chat_id": ...
    },
    "client_generated_id": ...,
    "notification_level": ...
  },
  "chat_message": {
    "message_content": {
      "segment": [
        {
          "type": "TEXT",
          "text": "hello, this is my message",
          "formatting": {
            "bold": false,
            "italics": false,
            "strikethrough": false,
            "underline": false
          }
        }
      ]
    }
  },
  "event_id": ...,
  "advances_sort_timestamp": ...,
  "event_otr": ...,
  "delivery_medium": {
    "medium_type": ...,
  },
  "event_type": "REGULAR_CHAT_MESSAGE",
  "event_version": ...
}
```
